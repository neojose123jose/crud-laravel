<?php

namespace App\Http\Controllers;

use App\aplicacion;
use Illuminate\Http\Request;

class AplicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aplicacion = Aplicacion::get();
        return json_encode($aplicacion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aplicacion = new Aplicacion();
        $aplicacion->nombres = $request->input('nombres');
        $aplicacion->apellidos = $request->input('apellidos');
        $aplicacion->correo = $request->input('correo');
        $aplicacion->telefono = $request->input('telefono');
        $aplicacion->direccion = $request->input('direccion');
        $aplicacion->save();
        


        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\aplicacion  $aplicacion
     * @return \Illuminate\Http\Response
     */
    public function show(aplicacion $aplicacion)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\aplicacion  $aplicacion
     * @return \Illuminate\Http\Response
     */
    public function edit(aplicacion $aplicacion)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\aplicacion  $aplicacion_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, aplicacion $aplicacion_id)
    {
        $aplicacion = Aplication::find($aplicacion_id);
        $aplicacion->nombres = $request->input('nombres');
        $aplicacion->apellidos = $request->input('apellidos');
        $aplicacion->correo = $request->input('correo');
        $aplicacion->telefono = $request->input('telefono');
        $aplicacion->direccion = $request->input('direccion');
        $aplicacion->save();
        json_encode($aplicacion);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\aplicacion  $aplicacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(aplicacion $aplicacion_id)
    {
        $aplicacion = Aplicacion::find($aplicacion_id);
        $aplicacion = delete();
    }
}
